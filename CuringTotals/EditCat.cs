﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuringTotals
{
    public partial class EditCat : Form
    {
        DataTable DT = new DataTable();
        public string Press { get; set; }
        public string CatNum { get; set; }
        public string  Comments { get; set; }
        public double Weight { get; set; }
        public int Shift { get; set; }
        public double Norm { get; set; }
        public EditCat()
        {
            InitializeComponent();
        }

        public EditCat(string press, int shift)
        {
            InitializeComponent();
            Press = press;
            Shift = shift;
        }

        private void EditCat_Load(object sender, EventArgs e)
        {
            Lbl_Press.Text += " " + Press;
            GetCatNumbers();
        }

        public void GetCatNumbers()
        {
            Form1 F = new Form1();
            DT = F.GetCatFromMCOVIP(Press, F.GetShiftNum());
            Weight = double.Parse(DT.Rows[0]["IWGHT"].ToString());
            CB_CatNumber.DataSource = DT;
            CB_CatNumber.DisplayMember = "OPRIT";
            CB_CatNumber.ValueMember = "OPRIT";
        }

        private void BTN_OK_Click(object sender, EventArgs e)
        {
            CatNum = CB_CatNumber.SelectedValue.ToString();
            Weight = double.Parse(DT.Select("OPRIT = '" + CatNum + "' ")[0]["IWGHT"].ToString());
            Norm = double.Parse(DT.Select("OPRIT = '" + CatNum + "' ")[0]["RMTKN"].ToString());
            Comments = Txt_Comment.Text;


            //// נשים את התקן של המק"ט החדש
            //DGV_Totals.Rows[e.RowIndex].Cells["Norm"].Value = string.IsNullOrEmpty(r["RMTKN"].ToString()) ? "0" : r["RMTKN"].ToString();


            // בדיקת שגיאות נוספת - עם המק"ט החדש
            //var selectedMach = Form1.SQLTables.Select($@"machine ={Press}");
            //Form1 F = new Form1();
            //Error = F.CheckForErrors(selectedMach[0][0].ToString(), CatNum, "");
            //if (err < 0)  // אם ישנה תקלה כלשהיא - נציג למנהל ונאפשר לו לעדכן את הנתונים
            //{
            //    row["counter"] = DBNull.Value;
            //}

            DialogResult = DialogResult.OK;

            //Form1 F = new Form1();
            //string AS400Cat = F.CheckIfFirstMCOVIP(Press,Shift);
            //if (AS400Cat == CatNum)
            //{
            //    Log L = new Log();
            //    L.CatNumFinal = CatNum;
            //    L.Shift = Shift;
            //    L.Press = Press;
            //    L.Date = F.GetCurrentShiftTimes().Item2;
            //    L.UpdateCatIsFirstRow();
            //}
            Close();
        }
    }
}

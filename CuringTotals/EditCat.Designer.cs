﻿namespace CuringTotals
{
    partial class EditCat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditCat));
            this.CB_CatNumber = new System.Windows.Forms.ComboBox();
            this.Lbl_Title = new System.Windows.Forms.Label();
            this.Lbl_Press = new System.Windows.Forms.Label();
            this.BTN_OK = new System.Windows.Forms.Button();
            this.Txt_Comment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // CB_CatNumber
            // 
            this.CB_CatNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CB_CatNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CB_CatNumber.FormattingEnabled = true;
            this.CB_CatNumber.Location = new System.Drawing.Point(39, 205);
            this.CB_CatNumber.Name = "CB_CatNumber";
            this.CB_CatNumber.Size = new System.Drawing.Size(207, 28);
            this.CB_CatNumber.TabIndex = 0;
            // 
            // Lbl_Title
            // 
            this.Lbl_Title.AutoSize = true;
            this.Lbl_Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Title.Location = new System.Drawing.Point(23, 120);
            this.Lbl_Title.Name = "Lbl_Title";
            this.Lbl_Title.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Title.Size = new System.Drawing.Size(256, 24);
            this.Lbl_Title.TabIndex = 1;
            this.Lbl_Title.Text = "אנא בחר את המק\"ט המתאים:";
            // 
            // Lbl_Press
            // 
            this.Lbl_Press.AutoSize = true;
            this.Lbl_Press.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Press.Location = new System.Drawing.Point(88, 178);
            this.Lbl_Press.Name = "Lbl_Press";
            this.Lbl_Press.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Press.Size = new System.Drawing.Size(68, 24);
            this.Lbl_Press.TabIndex = 2;
            this.Lbl_Press.Text = "מכבש: ";
            // 
            // BTN_OK
            // 
            this.BTN_OK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BTN_OK.Location = new System.Drawing.Point(96, 355);
            this.BTN_OK.Name = "BTN_OK";
            this.BTN_OK.Size = new System.Drawing.Size(85, 27);
            this.BTN_OK.TabIndex = 3;
            this.BTN_OK.Text = "אשר";
            this.BTN_OK.UseVisualStyleBackColor = true;
            this.BTN_OK.Click += new System.EventHandler(this.BTN_OK_Click);
            // 
            // Txt_Comment
            // 
            this.Txt_Comment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Comment.Location = new System.Drawing.Point(39, 277);
            this.Txt_Comment.MaxLength = 80;
            this.Txt_Comment.Multiline = true;
            this.Txt_Comment.Name = "Txt_Comment";
            this.Txt_Comment.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Txt_Comment.Size = new System.Drawing.Size(207, 72);
            this.Txt_Comment.TabIndex = 4;
            this.Txt_Comment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(110, 250);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(71, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "הערות:";
            // 
            // EditCat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CuringTotals.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(291, 395);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Txt_Comment);
            this.Controls.Add(this.BTN_OK);
            this.Controls.Add(this.Lbl_Press);
            this.Controls.Add(this.Lbl_Title);
            this.Controls.Add(this.CB_CatNumber);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditCat";
            this.Text = "EditCat";
            this.Load += new System.EventHandler(this.EditCat_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CB_CatNumber;
        private System.Windows.Forms.Label Lbl_Title;
        private System.Windows.Forms.Label Lbl_Press;
        private System.Windows.Forms.Button BTN_OK;
        private System.Windows.Forms.TextBox Txt_Comment;
        private System.Windows.Forms.Label label1;
    }
}
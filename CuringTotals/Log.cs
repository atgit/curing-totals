﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CuringTotals
{
    class Log
    {
        public DateTime Date { get; set; }
        public int Shift { get; set; }
        public string Press { get; set; }
        public string CatNumOriginal { get; set; }
        public string CatNumFinal { get; set; }
        public int AmountOrigin { get; set; }
        public int AmountFinal { get; set; }
        public bool Modified { get; set; }
        public string Comments { get; set; }
        public string EndSeries { get; set; }

        DBService dbs = new DBService();
        string StrSql = "";
        DataTable DT = new DataTable();

        public Log(DateTime date, int shift, string press, string catNumOrigin, string catNumFinal, int amountOrigin, int amountFinal, bool modified,string comments, string endSeries)
        {
            Date = date;
            Shift = shift;
            Press = press;
            CatNumOriginal = catNumOrigin;
            CatNumFinal = catNumFinal;
            AmountOrigin = amountOrigin;
            AmountFinal = amountFinal;
            Modified = modified;
            Comments = comments;
            EndSeries = EndSeries;
        }

        public Log()
        {

        }

        public Log(DateTime date, int shift,string press, string catNumFinal, int amountFinal, string comments, string endSereis)
        {
            Date = date;
            Shift = shift;
            Press = press;
            CatNumFinal = catNumFinal;
            AmountFinal = amountFinal;
            Comments = comments;
            EndSeries = endSereis;
        }

        public void AddToLog()
        {
            StrSql = $@"INSERT INTO TAPIALI.GIPLOG VALUES({ Date.ToString("yyyyMMdd")},'{Shift}','{Press}','{CatNumOriginal}','{CatNumFinal}',{AmountOrigin},{AmountFinal},'','{Comments}','{EndSeries}') ";
            dbs.executeInsertQuery(StrSql);
        }

        public void UpdateLog(string action)
        {
            //StrSql = $@"UPDATE TAPIALI.GIPLOG
            //           SET GPRDFIN='{CatNumFinal}', GQTYFIN={AmountFinal}, GCHANGE='Y', GCOMNT='{Comments}',GFINISH='{EndSeries}' 
            //           WHERE GDATE={Date.ToString("yyyyMMdd")} AND GSHIFT='{Shift}' AND GMACH='{Press}' ";
            StrSql = $@"UPDATE TAPIALI.GIPLOG a
                       SET a.GPRDFIN='{CatNumFinal}', a.GQTYFIN={AmountFinal}, a.GCHANGE='Y', a.GCOMNT='{Comments}',a.GFINISH='{EndSeries}' 
                       WHERE rrn(a) = (SELECT {action}(rrn(b)) FROM TAPIALI.GIPLOG b
	                    WHERE GDATE={Date.ToString("yyyyMMdd")} AND GSHIFT='{Shift}' AND GMACH='{Press}')";
            dbs.executeInsertQuery(StrSql);

            Form1 F = new Form1();
            string AS400Cat = F.CheckIfFirstMCOVIP(Press, Shift);
            if (AS400Cat == CatNumFinal)
            {
                UpdateCatIsFirstRow();
            }
        }

        public bool IsShiftInLog()
        {
            StrSql = $@"SELECT * FROM TAPIALI.GIPLOG WHERE GDATE = '{Date.ToString("yyyyMMdd")}' AND GSHIFT ='{Shift}' ";
            DataTable D = new DataTable();
            D = dbs.executeSelectQueryNoParam(StrSql);
            if (D.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// והמק'ט שהמנהל המשמרת בוחר זהים AS400 עדכון שדה מק'ט מקורי בלוג כך שכאשר המק'ט ב 
        /// נזין את המק'ט כך ששני העמודות יהיו זהות ולא יופיעו בדוחות הפערים
        /// </summary>
        public void UpdateCatIsFirstRow()
        {
            StrSql = $@"UPDATE TAPIALI.GIPLOG
                        SET GPRDORG='{CatNumFinal}' 
                        WHERE GDATE={Date.ToString("yyyyMMdd")} AND GMACH='{Press}' AND GSHIFT='{Shift}'";
            dbs.executeUpdateQuery(StrSql);
        }
    }
}

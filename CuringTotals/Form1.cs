﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CuringTotals
{
    public partial class Form1 : Form
    {
        public string StrSql = @"";
        DBService dbs = new DBService();
        DbServiceSQL dbsSql = new DbServiceSQL();
        DataTable DT = new DataTable();
        public static DataTable SQLTables = new DataTable();
        List<string> SQLTablesDual = new List<string>();
        Tuple<DateTime, DateTime> dates = new Tuple<DateTime, DateTime>(new DateTime(), new DateTime());
        SqlParameter[] param = new SqlParameter[10];
        string shift = "0";
        bool dataInLog = false;
        List<int> splitedCells = new List<int>();
        string Rzpali = "Rzpali";
        Task T;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dates = GetCurrentShiftTimes();
            shift = GetShiftNum();
        }

        private void סוףמשמרתToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Btn_Submit.Visible = true;
            Btn_Split.Visible = true;
            Lbl_CurrentShift.Visible = true;
            Lbl_Shift.Visible = true;
            DGV_Totals.Visible = true;
            DGV_Report.Visible = false;
            Lbl_From.Visible = false;
            Lbl_To.Visible = false;
            DTP_From.Visible = false;
            DTP_To.Visible = false;
            BTN_Send.Visible = false;
            Btn_Print.Visible = false;

            dataInLog = IsShiftInLog();
            GetTables();
            FillData();
            StyleDGV();
            // עדכון קובץ הלוג בצורה אסינכרונית
            T = new Task(() => AddDataToLog());
            T.Start();
        }

        private void דוחמסכםToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DGV_Totals.Visible = false;
            DGV_Report.Visible = true;
            Btn_Submit.Visible = false;
            Btn_Split.Visible = false;
            Lbl_From.Visible = true;
            Lbl_To.Visible = true;
            DTP_From.Visible = true;
            DTP_To.Visible = true;
            BTN_Send.Visible = true;
            Btn_Print.Visible = true;
            Lbl_Shift.Visible = false;

            DTP_From.Format = DateTimePickerFormat.Custom;
            DTP_From.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            DTP_To.Format = DateTimePickerFormat.Custom;
            DTP_To.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            DTP_From.Value = new DateTime(DateTime.Now.AddDays(-1).Year, DateTime.Now.AddDays(-1).Month, DateTime.Now.AddDays(-1).Day, 06, 30, 0);
            DTP_To.Value = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 30, 0);

        }

        private void BTN_Send_Click(object sender, EventArgs e)
        {
            Lbl_CurrentShift.Visible = true;
            //Lbl_Shift.Visible = true;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                Lbl_CurrentShift.Text = string.Format("דוח סיכום לתאריכים: {0}  עד  {1} ", DateTime.Now.AddDays(-3).ToShortDateString(), DateTime.Now.AddDays(-1).ToShortDateString());
            else
                Lbl_CurrentShift.Text = string.Format("דוח סיכום לתאריך  {0} ", DateTime.Now.AddDays(-1).ToShortDateString());

            DailyReport();
            StyleDGVReport();
        }


        public void StyleDGV()
        {
            DGV_Totals.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Totals.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Totals.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Totals.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 18F, GraphicsUnit.Pixel);
            foreach (DataGridViewColumn column in DGV_Totals.Columns)
            {
                column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
            }
            //DGV_Totals.Columns["Comments"].Visible = false;
            DGV_Totals.Columns["Weight"].Visible = false;

            DGV_Totals.Columns["mach"].Width = 150;
            DGV_Totals.Columns["CAT_NUM"].Width = 250;
            DGV_Totals.Columns["size"].Width = 150;
            DGV_Totals.Columns["Norm"].Width = 100;
            DGV_Totals.Columns["counter"].Width = 100;
            DGV_Totals.Columns["Weight"].Width = 150;
            DGV_Totals.Columns["EndSeries"].Width = 75;
            DGV_Totals.Columns["Comments"].Width = 850;
        }

        public void StyleDGVReport()
        {
            DGV_Report.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Report.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Report.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Report.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 18F, GraphicsUnit.Pixel);

            DGV_Report.Columns["Date"].HeaderText = "תאריך";
            DGV_Report.Columns["Shift"].HeaderText = "משמרת";
            DGV_Report.Columns["Mach"].HeaderText = "מכבש";
            DGV_Report.Columns["Original_Item"].HeaderText = "מק'ט מקורי";
            DGV_Report.Columns["Final_Item"].HeaderText = "מק'ט מעודכן";
            DGV_Report.Columns["Original_QTY"].HeaderText = "כמות מקורית";
            DGV_Report.Columns["Final_QTY"].HeaderText = "כמות מעודכנת";
            DGV_Report.Columns["Comments"].HeaderText = "הערות";
            DGV_Report.Columns["EndSeries"].HeaderText = "גמר סדרה";
            DGV_Report.Columns["IZREQ"].HeaderText = "מס דרישה";

            DGV_Report.Columns["Comments"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            DGV_Report.Columns["Comments"].Width = 600;

            foreach (DataGridViewColumn column in DGV_Report.Columns)
            {
                column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                if (column.DataPropertyName == "Original_Itam" || column.DataPropertyName == "Final_Itam")
                {
                    DGV_Report.Columns["CAT_NUM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
            }

            foreach (DataGridViewRow row in DGV_Report.Rows)
            {
                var D = DateTime.ParseExact(row.Cells[0].Value.ToString(), "yyyyMMdd", CultureInfo.CurrentCulture);
                row.Cells[0].Value = D.ToShortDateString();
            }
        }

        public void FillData()
        {
            DGV_Totals.Columns.Clear();
            DGV_Totals.Columns.Add("mach", "מכבש");
            DGV_Totals.Columns.Add("CAT_NUM", "מק'ט");
            DGV_Totals.Columns.Add("size", "גודל צמיג");
            DGV_Totals.Columns.Add("Norm", "תקן גיפור");
            DGV_Totals.Columns.Add("counter", "צמיגים שגופרו");
            DGV_Totals.Columns.Add("Weight", "משקל");
            DataGridViewCheckBoxColumn col = new DataGridViewCheckBoxColumn();
            col.HeaderText = "גמר סדרה";
            col.Name = "EndSeries";
            col.FalseValue = false;
            col.TrueValue = true;

            DGV_Totals.Columns.Add(col);
            DGV_Totals.Columns.Add("Comments", "הערות");


            foreach (DataRow mach in SQLTables.Rows)
            {
                //if (mach["machine"].ToString() == "414" )
                //{

                //}
                GetMachineData(mach["machine"].ToString());
            }

            foreach (string mach in SQLTablesDual)
            {
                GetMachineDualHeadData(mach.ToString());
            }
        }

        public void GetMachineData(string mach)
        {
            var selectedMach = SQLTables.Select($@"machine ={mach}");
            StrSql = $@"SELECT  PRESS_NUM as mach, CAT_NUM,Tire_Size as Size, 0 as Norm, COUNT(CAT_NUM) AS counter, '' as Weight, 0 as endSeries
                        FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate 
                       FROM {selectedMach[0][0]}) q 
                       where HOUR_END_CURE between '{dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000")}' and '{dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000")}' 
                       GROUP BY PRESS_NUM,CAT_NUM,Tire_Size";
            DT = dbsSql.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                DataTable D = GetCatFromMCOVIP(DT.Rows[0]["mach"].ToString());
                foreach (DataRow row in DT.Rows)
                 {
                    if (D.Rows.Count > 0)
                    {
                        //currentCat = D.Rows[0]["OPRIT"].ToString();
                        foreach (DataRow r in D.Rows)
                        {
                            if (r["OPRIT"].ToString() != row["CAT_NUM"].ToString())
                            {
                                row["CAT_NUM"] = DBNull.Value;
                            }
                            row["Weight"] = r["IWGHT"].ToString();
                            row["Norm"] = string.IsNullOrEmpty(r["RMTKN"].ToString()) ? "0" : r["RMTKN"].ToString();
                        }
                    }
                    else
                    {
                        row["CAT_NUM"] = DBNull.Value;
                    }

                    int err = CheckForErrors(selectedMach[0][0].ToString(), row["CAT_NUM"].ToString(), "");
                    if (err < 0)  // אם ישנה תקלה כלשהיא - נציג למנהל ונאפשר לו לעדכן את הנתונים
                    {
                        row["counter"] = DBNull.Value;
                    }

                    DGV_Totals.Rows.Add(row.ItemArray);
                }
            }
            else
            {
                DataGridViewRow R = (DataGridViewRow)DGV_Totals.Rows[0].Clone();
                DGV_Totals.Rows.Add(mach, DBNull.Value, DBNull.Value, 10, DBNull.Value, DBNull.Value);
            }
        }

        public void GetMachineDualHeadData(string mach)
        {
            #region
            //var selectedMach = SQLTablesDual.Select("machine = " + mach);
            //string side = "";

            ////StrSql = @"SELECT  PRESS_NUM as mach, CAT_NUM,COUNT(CAT_NUM) AS counter,sum(DATEDIFF(MINUTE, pDataDate, HOUR_ST_CURE)) as 'Waiting Time'
            ////            FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
            ////           "FROM " + selectedMach[0][0] + " ) q " +
            ////           "where HOUR_END_CURE between '" + dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000") + "' and '" + dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000") + "' " +
            ////           "GROUP BY PRESS_NUM,CAT_NUM";

            //for (int l = 0; l < 2; l++)
            //{
            //    side = l == 0 ? "L" : "R";

            //    StrSql = @"SELECT PRESS_NUM as mach,CAT_" + side + "_NUM,COUNT(CAT_" + side + "_NUM) AS counter " +
            //             "FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate " +
            //             "FROM " + selectedMach[0][0] + " ) q " +
            //             "Left join [dbo].[Open_Close_Norms] on press_no = PRESS_NUM " +
            //             "where HOUR_END_CURE between ? and ? " +
            //               "GROUP BY PRESS_NUM,CAT_" + side + "_NUM";
            //    SqlParameter d1 = new SqlParameter("date1", dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000"));
            //    SqlParameter d2 = new SqlParameter("date2", dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000"));
            //    DT = dbsSql.GetDataSetByQuery(StrSql,CommandType.Text,d1,d2).Tables[0];
            //    if (DT.Rows.Count > 0)
            //    {
            //        foreach (DataRow row in DT.Rows)
            //        {
            //            int err = CheckForErrors(selectedMach[0][0].ToString(), row["CAT_" + side + "_NUM"].ToString(), side);
            //            string currentMach = side == "L" ? row["mach"].ToString() : (int.Parse(row["mach"].ToString()) - 1).ToString(); 
            //            DGV_Totals.Rows.Add(currentMach, row["CAT_" + side + "_NUM"].ToString(), row["counter"].ToString());
            //            if (err < 0)  // אם ישנה תקלה כלשהיא - נציג למנהל ונאפשר לו לעדכן את הנתונים
            //            {
            //                DGV_Totals.CurrentRow.Cells["counter"].Style.BackColor = Color.PaleVioletRed;
            //            }
            //        }
            //    }
            //}
            #endregion
            DataTable CatNums = new DataTable();
            CatNums = GetCatFromMCOVIP(mach);
            foreach (DataRow row in CatNums.Rows)
            {
                DataGridViewRow R = (DataGridViewRow)DGV_Totals.Rows[0].Clone();
                DGV_Totals.Rows.Add(mach, row["OPRIT"].ToString(), row["INSIZ"].ToString(), row["RMTKN"].ToString(), DBNull.Value, row["IWGHT"].ToString());
            }
        }

        public DataTable GetCatFromMCOVIP(string mach)
        {
            DataTable DT = new DataTable();
            string date = dates.Item2.ToString("1yyMMdd");
            StrSql = $@"select DISTINCT OPRIT,ICSCP1 as IWGHT, floor(510 / (1440 / RMTKN)) as RMTKN,INSIZ
                        from rzpali.mcovip
	                    LEFT JOIN BPCSFV30.CICL01 ON ICPROD=OPRIT 
                        LEFT JOIN BPCSFALI.IIMN ON INPROD=substring(OPRIT,1,8)
                        LEFT JOIN BPCSFALI.FRTML01 ON RMPROD=OPRIT AND RMMACI='{mach}'
                        WHERE ODATE ={ date} AND OMACH='{mach}' AND OSHIFT= '{shift}' AND substring(OPRIT,1,4) <> 'STOP' AND ICFAC='F1' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT;
            }
            return new DataTable();
        }
        /// <summary>
        /// GetCatFromMCOVIP Overload 
        /// </summary>
        /// <param name="mach"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public DataTable GetCatFromMCOVIP(string mach, string shift)
        {
            string date = GetCurrentShiftTimes().Item2.ToString("1yyMMdd");
            StrSql = $@"select DISTINCT OPRIT ,ICSCP1 as IWGHT, floor(510 / (1440 / RMTKN)) as RMTKN
                        from rzpali.mcovip
                        LEFT JOIN BPCSFV30.CICL01 ON ICPROD=OPRIT 
                        LEFT JOIN BPCSFALI.FRTML01 ON RMPROD=OPRIT AND RMMACI='{mach}'
                        WHERE ODATE =" + date + " AND OMACH='" + mach + "' AND OSHIFT= '" + shift + "' AND substring(OPRIT,1,4) <> 'STOP' AND ICFAC='F1' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return DT;
            }
            return new DataTable();
        }

        public void GetTables()
        {
            // יצירת טבלאת כלל הטבלאות שבבסיס הנתונים ומילוי נתונים לקומבובוקסים
            StrSql = "SELECT TABLE_NAME, substring(TABLE_NAME,10,3) as machine " +
                    "FROM INFORMATION_SCHEMA.TABLES " +
                    "WHERE TABLE_TYPE = 'BASE TABLE'  AND TABLE_CATALOG = 'PRESERVER' and TABLE_NAME like '%DATA%' and TABLE_NAME not in ('DATA_162_301_63','DATA_162_307_55','DATA_162_322_655','DATA_162_303_63','DATA_162_311_63','DATA_162_313_63','PROGRAM-DATA','PROGRAM-DATA-TEST') " +
                    "order by substring(TABLE_NAME,10,3) ";
            SQLTables = dbsSql.executeSelectQueryNoParam(StrSql);

            // StrSql = "SELECT TABLE_NAME, substring(TABLE_NAME,10,3) as machine " +
            //        "FROM INFORMATION_SCHEMA.TABLES " +
            //        "WHERE TABLE_TYPE = 'BASE TABLE'  AND TABLE_CATALOG = 'PRESERVER' and TABLE_NAME like '%DATA%' and TABLE_NAME in ('DATA_162_301_63','DATA_162_307_55','DATA_162_322_655') " +
            //        "order by substring(TABLE_NAME,10,3) ";
            //SQLTablesDual = dbsSql.executeSelectQueryNoParam(StrSql);
            SQLTablesDual.Add("301");
            SQLTablesDual.Add("302");
            SQLTablesDual.Add("303");
            SQLTablesDual.Add("304");
            SQLTablesDual.Add("307");
            SQLTablesDual.Add("308");
            SQLTablesDual.Add("311");
            SQLTablesDual.Add("312");
            SQLTablesDual.Add("313");
            SQLTablesDual.Add("314");
            SQLTablesDual.Add("322");
            SQLTablesDual.Add("323");
        }

        /// <summary>
        /// הפונקציה מחזירה את שני התאריכים הרלוונטיים לשליפת נתוני המשמרת הקודמת
        /// </summary>
        /// <returns></returns>
        public Tuple<DateTime, DateTime> GetCurrentShiftTimes()
        {
            if (DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 30, 00)) >= 0 || DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 30, 00)) <= 0)
            {
                shift = "3";
                DateTime F = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 00, 00);
                DateTime T = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 29, 59);

                if (DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 30, 00)) <= 0) // אם אחרי 0:00 בלילה - נחסיר יום
                {
                    F = F.AddDays(-1);
                    T = T.AddDays(-1);
                }

                return new Tuple<DateTime, DateTime>(F, T);

            }
            else if (DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 00, 00)) >= 0)
            {
                shift = "2";
                return new Tuple<DateTime, DateTime>(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 30, 00), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 59, 59));
            }
            else
            {
                shift = "1";
                DateTime D = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                D = D.AddDays(-1);
                TimeSpan T = new TimeSpan(23, 30, 00);
                D = D.Date + T;
                return new Tuple<DateTime, DateTime>(D, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 29, 59));
                //return new Tuple<DateTime, DateTime>(new DateTime(2018,05,20,23,30,0), new DateTime(2018,05,21,06,30,0));
            }
            // Tests
            //shift = "3";
            //return new Tuple<DateTime, DateTime>(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 15, 00, 00), new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(-1).Day, 23, 30, 00));
            //return new Tuple<DateTime, DateTime>(new DateTime(2018,05,02,23,30,0), new DateTime(2018,05,03,06,30,0));
        }

        public string GetShiftNum()
        {
            if (DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 30, 00)) >= 0 || DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 06, 30, 00)) <= 0)
            {
                Lbl_CurrentShift.Text = DateTime.Now.Hour == 23 ? DateTime.Now.ToShortDateString() + " - משמרת " + shift : DateTime.Now.AddDays(-1).ToShortDateString() + " - משמרת " + shift;
                Lbl_Shift.Text = "משמרת ערב";
                return "3";
            }
            else if (DateTime.Compare(DateTime.Now, new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 15, 00, 00)) >= 0)
            {
                Lbl_CurrentShift.Text = DateTime.Now.ToShortDateString() + " - משמרת " + shift;
                Lbl_Shift.Text = "משמרת בוקר";
                return "2";
            }
            else
            {
                Lbl_CurrentShift.Text = DateTime.Now.ToShortDateString() + " - משמרת " + shift;
                Lbl_Shift.Text = "משמרת לילה";
                return "1";
            }
        }

        /// <summary>
        /// פונקציה הבודקת האם היו שגיאות בדיווחים למכבש
        /// </summary>
        /// <param name="tbl"></param>
        /// <returns></returns>
        public int CheckForErrors(string tbl, string cat, string side)
        {
            double curingTime = 0;
            // בדיקת תחילת גיפור אחרי סוף גיפור
            StrSql = @"SELECT *
                        FROM " + tbl + @"
                        WHERE HOUR_ST_CURE > HOUR_END_CURE AND HOUR_END_CURE between ? and ? ";
            SqlParameter d1 = new SqlParameter("date1", dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000"));
            SqlParameter d2 = new SqlParameter("date2", dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000"));
            DT = dbsSql.GetDataSetByQuery(StrSql, CommandType.Text, d1, d2).Tables[0];
            if (DT.Rows.Count > 0)
            {
                return -1;
            }

            // בדיקת זמן תחילת גיפור קטן מזמן סיום גיפור של צמיג קודם
            StrSql = @"SELECT *
                        FROM " + tbl + @" b
                        WHERE b.HOUR_ST_CURE <
                        (SELECT max(a.HOUR_ST_CURE) FROM " + tbl + @" a WHERE a.HOUR_ST_CURE < b.HOUR_END_CURE AND b.HOUR_END_CURE between ? and ?)";
            d1 = new SqlParameter("date1", dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000"));
            d2 = new SqlParameter("date2", dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000"));
            DT = dbsSql.GetDataSetByQuery(StrSql, CommandType.Text, d1, d2).Tables[0];
            if (DT.Rows.Count > 0)
            {
                return -1;
            }

            // זמן גיפור חריג
            StrSql = @"SELECT SPROD,GCCODE,GCTIME
                        FROM  BPCSFALI.IIMS
                        LEFT JOIN mfrt.gprcp ON SGPRC=GCCODE 
                        WHERE GCMIF = 'F1' AND SPROD ='" + cat + "'";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                curingTime = double.Parse(DT.Rows[0]["GCTIME"].ToString());
            }
            string catNu = side == "" ? "CAT_NUM" : "CAT_" + side + "_NUM";  // התאמת המשתנה למכבשים כפולים
            StrSql = $@"SELECT * 
                       FROM {tbl}
                       WHERE HOUR_END_CURE between ? and ? AND {catNu} = ? AND DATEDIFF(minute,HOUR_ST_CURE,HOUR_END_CURE) NOT BETWEEN ? AND ? ";
            SqlParameter c = new SqlParameter("c", cat);
            SqlParameter tm = new SqlParameter("t", curingTime - 3);
            SqlParameter t = new SqlParameter("t", curingTime + 20);
            DT = dbsSql.GetDataSetByQuery(StrSql, CommandType.Text, d1, d2, c, tm, t).Tables[0];
            if (DT.Rows.Count > 0 || curingTime == 0)
            {
                return -1;
            }
            return 0;
        }

        private void DGV_Totals_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                e.Cancel = true;
            }
            else if (e.ColumnIndex == 4 && DGV_Totals.Rows[e.RowIndex].Cells["counter"].Value == DBNull.Value || DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.PaleVioletRed)
            {
                e.Cancel = false;
                DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.LightGreen;
            }
            else if (e.ColumnIndex == 4 && DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.LightGreen || DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.PaleVioletRed)
            {
                e.Cancel = false;
            }
            else if (e.ColumnIndex == 6 || e.ColumnIndex == 7)
            {
                e.Cancel = false;
                DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.LightGreen;
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void DGV_Totals_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (DGV_Totals.Rows[e.RowIndex].Cells["counter"].Value == DBNull.Value || DGV_Totals.Rows[e.RowIndex].Cells["counter"].Value == null)
            {
                DGV_Totals.Rows[e.RowIndex].Cells["counter"].Style.BackColor = Color.PaleVioletRed;
            }

            if (DGV_Totals.Rows[e.RowIndex].Cells["CAT_NUM"].Value == DBNull.Value)
            {
                DGV_Totals.Rows[e.RowIndex].Cells["CAT_NUM"].Style.BackColor = Color.PaleVioletRed;
            }
        }

        public void AddDataToLog()
        {
            if (!dataInLog)
            {
                DataTable Data = new DataTable();
                Data.Columns.Add("mach");
                Data.Columns.Add("CAT_NUM");
                Data.Columns.Add("SIZE");
                Data.Columns.Add("counter");

                foreach (DataGridViewRow row in DGV_Totals.Rows)
                {
                    DataRow R = Data.NewRow();
                    R["mach"] = row.Cells["mach"].Value;
                    R["CAT_NUM"] = row.Cells["CAT_NUM"].Value;
                    R["SIZE"] = row.Cells["SIZE"].Value;
                    R["counter"] = row.Cells["counter"].Value;
                    Data.Rows.Add(R);
                }

                foreach (DataRow row in Data.Rows)
                {
                    int counter = row["counter"].ToString() == "" ? 0 : int.Parse(row["counter"].ToString());
                    AddToLog(row["mach"].ToString(), row["CAT_NUM"].ToString(), "", counter, 0, "", false);
                }
            }
        }

        public void AddToLog(string press, string originCat, string finalCat, int originQTY, int finalQTY, string comments, bool endSeries)
        {
            string end = endSeries ? "Y" : "";
            Log L = new Log(dates.Item2, int.Parse(shift), press, originCat, finalCat, originQTY, finalQTY, false, comments, end);
            L.AddToLog();
        }

        public bool IsShiftInLog()
        {
            Log L = new Log();
            L.Date = dates.Item2;
            L.Shift = int.Parse(GetShiftNum());
            return L.IsShiftInLog();
        }

        public void UpdateLog(string press, string finalCat, int finalQTY, string comments, bool endSeries, string action)
        {
            string end = endSeries ? "Y" : "";
            Log L = new Log(dates.Item2, int.Parse(shift), press, finalCat, finalQTY, comments, end);
            L.UpdateLog(action);
        }

        private void Btn_Submit_Click(object sender, EventArgs e)
        {
            bool approved = true;
            foreach (DataGridViewRow row in DGV_Totals.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.Style.BackColor == Color.PaleVioletRed)
                    {
                        approved = false;
                    }
                }
            }

            if (approved)
            {
                double TotalWeight = 0;
                if (ClearMCOVIP()) // אם הרשומות בקובץ עוד לא עברו למלאי - נעדכן את הקובץ
                {
                    WaitForm Wa = new WaitForm();
                    Wa.Show();
                    Application.DoEvents();

                    double t = 0;
                    CopyMCOVIPToDemo();
                    string prev = "";
                    foreach (DataGridViewRow row in DGV_Totals.Rows)
                    {
                        try
                        {
                            // עדכון הקובץ
                            UpdateMCOVIP(row.Cells["mach"].Value.ToString(), row.Cells["CAT_NUM"].Value.ToString(), int.Parse(row.Cells["counter"].Value.ToString()));
                            t = !string.IsNullOrEmpty(row.Cells["Weight"].Value.ToString() ?? "") ? double.Parse(row.Cells["Weight"].Value.ToString()) * double.Parse(row.Cells["counter"].Value.ToString()) : 0;

                            TotalWeight += !string.IsNullOrEmpty(row.Cells["Weight"].ToString() ?? "") ? double.Parse(row.Cells["Weight"].Value.ToString()) * double.Parse(row.Cells["counter"].Value.ToString()) : 0;

                            if (row.Cells["CAT_NUM"].Style.BackColor == Color.LightGreen || row.Cells["counter"].Style.BackColor == Color.LightGreen || row.Cells["Comments"].Style.BackColor == Color.LightGreen || row.Cells["EndSeries"].Style.BackColor == Color.LightGreen)  // בדיקה האם השורה עודכנה
                            {
                                string comm = string.IsNullOrEmpty(row.Cells["Comments"].Value as string) ? "" : row.Cells["Comments"].Value.ToString();

                                // אם למכבש יש שני דיווחים - נפצל את השאילתות כדי לעדכן רשומה רשומה
                                if (row.Cells["mach"].Value.ToString() == prev)
                                {
                                    // עדכון קובץ הלוג
                                    UpdateLog(row.Cells["mach"].Value.ToString(), row.Cells["CAT_NUM"].Value.ToString(), int.Parse(row.Cells["counter"].Value.ToString()), comm, Convert.ToBoolean(row.Cells["EndSeries"].Value), "MIN");
                                }
                                else
                                {
                                    // עדכון קובץ הלוג
                                    UpdateLog(row.Cells["mach"].Value.ToString(), row.Cells["CAT_NUM"].Value.ToString(), int.Parse(row.Cells["counter"].Value.ToString()), comm, Convert.ToBoolean(row.Cells["EndSeries"].Value), "MAX");
                                }

                            }

                            prev = row.Cells["mach"].Value.ToString();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                    Wa.Close();
                    TotalWeight = Math.Round(TotalWeight / 1000, 1);
                    MessageBox.Show(string.Format("במשמרת זו דיווחת בהצלחה על {0} טון", TotalWeight.ToString()));
                    DGV_Totals.ForeColor = Color.Gray;
                    DGV_Totals.ColumnHeadersDefaultCellStyle.ForeColor = Color.Gray;
                    DGV_Totals.EnableHeadersVisualStyles = false;
                    Btn_Submit.Enabled = false;

                    MailToTapi(shift, dates.Item2);
                    //Close();
                }
            }
            else
            {
                MessageBox.Show(".נותרו עוד שדות רקים. אנא מלא אותם ושגר שוב");
            }
        }

        public bool ClearMCOVIP()
        {
            StrSql = $@"SELECT * FROM RZPALI.MCOVIP
                        WHERE ODATE={dates.Item2.ToString("1yyMMdd")} AND (ODEPE = 161 OR ODEPE = 162) AND OSHIFT = {shift} AND OMADE >= 1 AND OSTTS <> 0"; // בדיקה האם המשמרת כבר שודרה
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count == 0)
            {
                StrSql = $@"UPDATE {Rzpali}.MCOVIP
                        SET OMADE = 0 
                        WHERE ODATE={dates.Item2.ToString("1yyMMdd")} AND (ODEPE = 161 OR ODEPE = 162) AND OSHIFT = {shift} AND OMADE >= 1 ";
                dbs.executeUpdateQuery(StrSql);
                return true;
            }
            else
            {
                MessageBox.Show(".בוצע שידור למשמרת המבוקשת. אנא פנה למחלקת התפ'י לעזרה");
                return false;
            }
        }

        public void UpdateMCOVIP(string press, string finalCat, int finalQTY)
        {
            StrSql = $@"UPDATE {Rzpali}.MCOVIP
                        SET OMADE = {finalQTY} 
                        WHERE ODATE={dates.Item2.ToString("1yyMMdd")} AND (ODEPE = 161 OR ODEPE = 162) AND OSHIFT = {shift} AND OPRIT = '{finalCat}' AND OMACH='{press}'";
            dbs.executeUpdateQuery(StrSql);
        }

        /// <summary>
        /// פתיחת חלונית עדכון מק"ט 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DGV_Totals_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 1 && (DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.LightGreen || DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.PaleVioletRed))
            {
                using (EditCat form2 = new EditCat(DGV_Totals.Rows[e.RowIndex].Cells["mach"].Value.ToString(), int.Parse(shift)))
                {
                    if (form2.ShowDialog() == DialogResult.OK)
                    {
                        DGV_Totals.Rows[e.RowIndex].Cells["CAT_NUM"].Value = form2.CatNum;
                        DGV_Totals.Rows[e.RowIndex].Cells["Comments"].Value = form2.Comments;
                        DGV_Totals.Rows[e.RowIndex].Cells["Weight"].Value = form2.Weight;
                        DGV_Totals.Rows[e.RowIndex].Cells["Norm"].Value = form2.Norm;
                        //if (form2.Error < 0)
                        //{

                        //}
                        //DGV_Totals.Rows[e.RowIndex].Cells["counter"].Value = DBNull.Value;
                        DGV_Totals.Rows[e.RowIndex].Cells["CAT_NUM"].Style.BackColor = Color.LightGreen;

                        // בדיקת תקלות
                        var selectedMach = SQLTables.Select($@"machine ={DGV_Totals.Rows[e.RowIndex].Cells["mach"].Value}");

                        // נבדוק שוב מה המק'ט השגוי כדי לתפוס את הגיפורים בהם הוא דווח
                        StrSql = $@"SELECT  PRESS_NUM as mach, CAT_NUM,Tire_Size as Size, 0 as Norm, COUNT(CAT_NUM) AS counter, '' as Weight, 0 as endSeries
                        FROM(SELECT *,LAG(HOUR_END_CURE) OVER(ORDER BY HOUR_END_CURE) pDataDate 
                       FROM {selectedMach[0][0]}) q 
                       where HOUR_END_CURE between '{dates.Item1.ToString("yyyy-MM-dd HH:mm:00.000")}' and '{dates.Item2.ToString("yyyy-MM-dd HH:mm:00.000")}' 
                       GROUP BY PRESS_NUM,CAT_NUM,Tire_Size";
                        DT = dbsSql.executeSelectQueryNoParam(StrSql);
                        if (DT.Rows.Count > 0)
                        {
                            foreach (DataRow row in DT.Rows)
                            {
                                DataTable D = GetCatFromMCOVIP(row["mach"].ToString());

                                if (D.Rows.Count > 0)
                                {
                                    foreach (DataRow r in DT.Rows)
                                    {
                                        if (D.Rows[0]["OPRIT"].ToString() != row["CAT_NUM"].ToString())
                                        {
                                            int err = CheckForErrors(selectedMach[0][0].ToString(), row["CAT_NUM"].ToString(), "");
                                            if (err < 0)  // אם ישנה תקלה כלשהיא - נציג למנהל ונאפשר לו לעדכן את הנתונים
                                            {
                                                DGV_Totals.Rows[e.RowIndex].Cells["counter"].Value = DBNull.Value;
                                                DGV_Totals.Rows[e.RowIndex].Cells["counter"].Style.BackColor = Color.PaleVioletRed;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void DailyReport()
        {
            StrSql = $@"SELECT distinct char(GDATE) as Date,GMACH as mach,IZREQ, GSHIFT as Shift, GPRDORG as Original_Item,GPRDFIN as Final_Item, GQTYORG as Original_QTY, GQTYFIN as Final_QTY, GCOMNT as Comments, GFINISH as EndSeries 
              FROM TAPIALI.GIPLOG 
              LEFT JOIN BPCSFALI.SHMAL01 ON GMACH=IZMACH AND GPRDFIN=IZPROD
                           WHERE (GDATE||GSHIFT >= {DTP_From.Value.ToString("yyyyMMdd") + 2} AND GDATE||GSHIFT <= {DTP_To.Value.ToString("yyyyMMdd") + 1} AND GCHANGE <> '' AND GPRDORG <> GPRDFIN ) OR (GCOMNT<>'' AND GDATE||GSHIFT >= {DTP_From.Value.ToString("yyyyMMdd") + 2} AND GDATE||GSHIFT <= {DTP_To.Value.ToString("yyyyMMdd") + 1}) OR (GDATE||GSHIFT >= {DTP_From.Value.ToString("yyyyMMdd") + 2} AND GDATE||GSHIFT <= {DTP_To.Value.ToString("yyyyMMdd") + 1} AND GFINISH<>'' ) 
                           -- order by GMACH,GDATE,SHIFT ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            DGV_Report.DataSource = DT;
        }

        public void CopyMCOVIPToDemo()
        {
            string date = dates.Item2.ToString("1yyMMdd");
            StrSql = $@"DELETE FROM RZPADALI.MCOVIP 
                        WHERE ODATE = {date} AND OSHIFT='{shift}'";
            dbs.executeUpdateQuery(StrSql);

            StrSql = $@"INSERT INTO RZPADALI.MCOVIP (SELECT * FROM RZPALI.MCOVIP
                        WHERE ODATE = {date} AND OSHIFT='{shift}')";
            dbs.executeUpdateQuery(StrSql);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DGV_Totals_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                if (!string.IsNullOrEmpty(e.FormattedValue.ToString()) && e.FormattedValue.ToString() != "0")
                {
                    if (int.Parse(DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].EditedFormattedValue.ToString()) - int.Parse(DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex - 1].Value.ToString()) > 1) // בדיקה אם מוזנת כמות גדולה מהתקן 
                    {
                        DGV_Totals.Rows[e.RowIndex].ErrorText = "!הכמות המוזנת גבוהה מהתקן";
                        DGV_Totals.CurrentRow.Cells[e.ColumnIndex].Style.BackColor = Color.PaleVioletRed;
                    }
                }
                else if (string.IsNullOrEmpty(e.FormattedValue.ToString()) && DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor == Color.LightGreen)
                {
                    DGV_Totals.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.PaleVioletRed;
                    //e.Cancel = true;
                }
            }
        }

        private void DGV_Report_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                e.AdvancedBorderStyle.Bottom = DataGridViewAdvancedCellBorderStyle.None;
                if (e.RowIndex < 1 || e.ColumnIndex < 0)
                    return;

                if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
                {
                    e.AdvancedBorderStyle.Top = DataGridViewAdvancedCellBorderStyle.None;
                }
                else
                {
                    e.AdvancedBorderStyle.Top = DGV_Report.AdvancedCellBorderStyle.Top;
                }
            }

            foreach (DataGridViewRow row in DGV_Report.Rows)
            {
                if (row.Cells["Original_Item"].Value.ToString() != row.Cells["Final_Item"].Value.ToString())
                {
                    row.Cells["Original_Item"].Style.BackColor = Color.PaleVioletRed;
                    row.Cells["Final_Item"].Style.BackColor = Color.PaleVioletRed;
                }
                if (row.Cells["Original_QTY"].Value.ToString() != row.Cells["Final_QTY"].Value.ToString())
                {
                    row.Cells["Original_QTY"].Style.BackColor = Color.PaleVioletRed;
                    row.Cells["Final_QTY"].Style.BackColor = Color.PaleVioletRed;
                }
                if (row.Cells["Comments"].Value.ToString() != "")
                {
                    row.Cells["Comments"].Style.BackColor = Color.PaleVioletRed;
                }
                if (row.Cells["EndSeries"].Value.ToString() != "")
                {
                    row.Cells["EndSeries"].Style.BackColor = Color.Yellow;
                }
            }
        }

        bool IsTheSameCellValue(int column, int row)
        {
            DataGridViewCell cell1 = DGV_Report[column, row];
            DataGridViewCell cell2 = DGV_Report[column, row - 1];
            if (cell1.Value == null || cell2.Value == null)
            {
                return false;
            }
            return cell1.Value.ToString() == cell2.Value.ToString();
        }

        private void DGV_Report_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex == 0)
                return;
            if (e.ColumnIndex == 1)
            {
                if (IsTheSameCellValue(e.ColumnIndex, e.RowIndex))
                {
                    e.Value = "";
                    e.FormattingApplied = true;
                }
            }
        }

        public string CheckIfFirstMCOVIP(string press, int shft)
        {
            string catNum = "";
            string date = GetCurrentShiftTimes().Item2.ToString("1yyMMdd");
            StrSql = $@"SELECT OPRIT FROM {Rzpali}.MCOVIP
                        WHERE OMACH = '{press}' AND ODATE = '{date}' AND OSHIFT = '{shft}' AND OADIF = '1' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                catNum = DT.Rows[0]["OPRIT"].ToString();
            }
            return catNum;
        }

        private void Btn_Split_Click(object sender, EventArgs e)
        {
            if (DGV_Totals.CurrentCell != null)
            {
                int t = DGV_Totals.CurrentCell.RowIndex;
                DataTable Cats = GetCatFromMCOVIP(DGV_Totals.Rows[t].Cells["mach"].Value.ToString());

                if (Cats.Rows.Count > 1) // האם יש יותר ממקט אחד ב מערכת?
                {
                    DataTable DT1 = new DataTable();

                    for (int i = 0; i < DGV_Totals.Columns.Count; i++)
                    {
                        DT1.Columns.Add(DGV_Totals.Columns[i].Name);
                    }

                    foreach (DataGridViewRow row in DGV_Totals.Rows)
                    {
                        var x = DT1.NewRow();

                        for (int j = 0; j < row.Cells.Count; j++)
                        {
                            x[j] = row.Cells[j].Value;
                        }
                        DT1.Rows.Add(x);
                    }

                    DataRow R = DT1.NewRow(); // הוספת שורה חדשה

                    R["mach"] = DT1.Rows[t]["mach"]; // 
                    R["CAT_NUM"] = DT1.Rows[t]["CAT_NUM"]; // הוספת שאר הנתונים שניקח מהשורה שממנה פיצלנו
                    R["size"] = DT1.Rows[t]["size"];
                    R["Norm"] = DT1.Rows[t]["Norm"];
                    R["counter"] = DBNull.Value;
                    R["Weight"] = DT1.Rows[t]["Weight"];
                    R["EndSeries"] = false;
                    R["Comments"] = string.IsNullOrEmpty(DT1.Rows[t]["Comments"].ToString()) ? "" : DT1.Rows[t]["Comments"].ToString();

                    splitedCells.Add(t);
                    DGV_Totals.Rows.Insert(t, R.ItemArray);

                    DGV_Totals.Rows[t].Cells["counter"].Value = DBNull.Value; // לאפשר למנהל לדווח כמות
                    DGV_Totals.Rows[t + 1].Cells["counter"].Value = DBNull.Value; // לאפשר למנהל לדווח כמות
                    DGV_Totals.Rows[t + 1].Cells["counter"].Style.BackColor = Color.PaleVioletRed;

                    // השמת המק"ט המתאים בשתי השורות
                    DGV_Totals.Rows[t].Cells["CAT_NUM"].Value = Cats.Rows[0]["OPRIT"].ToString();
                    DGV_Totals.Rows[t + 1].Cells["CAT_NUM"].Value = Cats.Rows[1]["OPRIT"].ToString();
                    DGV_Totals.Rows[t].Cells["CAT_NUM"].Style.BackColor = Color.LightGreen;
                    DGV_Totals.Rows[t + 1].Cells["CAT_NUM"].Style.BackColor = Color.LightGreen;

                    DGV_Totals.Refresh();
                }
            }
        }

        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (DGV_Report.Rows.Count > 0)
            {
                Microsoft.Office.Interop.Excel.Application xlexcel;
                Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;
                xlexcel = new Microsoft.Office.Interop.Excel.Application();
                Microsoft.Office.Interop.Excel.Range chartRange;

                xlWorkBook = xlexcel.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                Microsoft.Office.Interop.Excel.Range CR = xlWorkBook.Worksheets[1].Cells;
                CR.NumberFormat = "@";

                // מירכוז + פונט מודגש לתחום            
                chartRange = xlWorkSheet.get_Range("A1", "J1");
                chartRange.HorizontalAlignment = 3;
                chartRange.VerticalAlignment = 3;
                chartRange.Font.Bold = true;

                chartRange = xlWorkSheet.get_Range("A:A");
                chartRange.ColumnWidth = 15;
                chartRange = xlWorkSheet.get_Range("C:C");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("D:D");
                chartRange.ColumnWidth = 10;
                chartRange = xlWorkSheet.get_Range("E:F");
                chartRange.ColumnWidth = 18;
                chartRange = xlWorkSheet.get_Range("G:H");
                chartRange.ColumnWidth = 15;

                int i = 1;
                foreach (DataGridViewColumn item in DGV_Report.Columns)
                {
                    if (item.Name != "COMMENTS" && item.Name != "ORIGINAL_QTY" && item.Name != "FINAL_QTY")
                    {
                        xlWorkSheet.Cells[1, i] = item.HeaderText;
                        i++;
                    }
                }

                int j = 2;
                int y = 1;
                foreach (DataGridViewRow row in DGV_Report.Rows)
                {
                    i = 1;
                    y = 1;
                    foreach (DataGridViewColumn item in DGV_Report.Columns)
                    {
                        if (item.Name != "COMMENTS" && item.Name != "ORIGINAL_QTY" && item.Name != "FINAL_QTY")
                        {
                            xlWorkSheet.Cells[j, y] = row.Cells[i - 1].Value;
                            y++;
                        }
                        i++;
                    }
                    j++;
                }


                var _with1 = xlWorkSheet.PageSetup;
                // A4 papersize
                _with1.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4;
                // Landscape orientation
                _with1.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                _with1.FitToPagesWide = 1;
                _with1.PrintGridlines = true;

                xlWorkSheet.PrintOutEx(Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                xlWorkBook.Close(false, misValue, misValue);
                xlexcel.Quit();
                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlexcel);
            }
            else
            {
                MessageBox.Show(".לא נבחרו נתונים לייצוא. אנא בחר שנית");
            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void MailToTapi(string shift, DateTime date)
        {
            DBService dbs = new DBService();
            // שליחת מייל לתפי
            string Sbjct = string.Format("דווחו כמויות לתאריך {0} משמרת {1}", date.ToShortDateString(), shift);
            MailMessage message = new MailMessage();
            //message.To.Add(new MailAddress("mwahby@atgtire.com"));

            // שליפת רשימת תפוצה
            string connectionString = "Provider=Microsoft.JET.OLEDB.4.0;data source=T:\\Application\\C#\\Mailing_Address\\Mail.mdb";
            OleDbConnection conn = new OleDbConnection(connectionString);
            // To Query
            string sql = "SELECT Email FROM Mail where Subject='CuringTotals' and ucase(Dest)='TO'";
            OleDbCommand cmd = new OleDbCommand(sql, conn);
            conn.Open();
            OleDbDataReader reader;
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                message.To.Add(new MailAddress((reader.GetString(0).ToString().Trim())));
            }

            reader.Close();
            // CC Query
            sql = "SELECT Email FROM Mail where Subject='CuringTotals' and ucase(Dest)='CC'";
            cmd = new OleDbCommand(sql, conn);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                message.CC.Add(new MailAddress((reader.GetString(0).ToString().Trim())));
            }
            reader.Close();
            // From Query
            sql = "SELECT Email FROM Mail where Subject='CuringTotals' and ucase(Dest)='FROM'";
            cmd = new OleDbCommand(sql, conn);
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                message.From = new MailAddress(reader.GetString(0).ToString().Trim());
            }
            reader.Close();
            conn.Close();

            message.Subject = Sbjct;
            message.Body = "בוצע עדכון כמויות במחלקת הגיפור למשמרת " + shift + " לתאריך " + date.ToShortDateString();
            message.Priority = MailPriority.High;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.SubjectEncoding = System.Text.Encoding.UTF8;
            SmtpClient client = new SmtpClient();
            client.Host = "almail";   // ServerIP;
            client.Send(message);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if ((DateTime.Now.Hour == 6 || DateTime.Now.Hour == 23) && DateTime.Now.Minute == 30)
            {
                if (T.IsCompleted)
                {
                    this.Close();
                }

            }
            else if (DateTime.Now.Hour == 15 && DateTime.Now.Minute == 0)
            {
                if (T.IsCompleted)
                {
                    this.Close();
                }
            }
        }
    }
}

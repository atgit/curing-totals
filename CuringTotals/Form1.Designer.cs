﻿namespace CuringTotals
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.סוףמשמרתToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.דוחמסכםToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DGV_Totals = new System.Windows.Forms.DataGridView();
            this.Lbl_CurrentShift = new System.Windows.Forms.Label();
            this.Btn_Submit = new System.Windows.Forms.Button();
            this.DGV_Report = new System.Windows.Forms.DataGridView();
            this.Lbl_To = new System.Windows.Forms.Label();
            this.Lbl_From = new System.Windows.Forms.Label();
            this.DTP_From = new System.Windows.Forms.DateTimePicker();
            this.DTP_To = new System.Windows.Forms.DateTimePicker();
            this.BTN_Send = new System.Windows.Forms.Button();
            this.Btn_Split = new System.Windows.Forms.Button();
            this.Btn_Print = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Lbl_Shift = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Totals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Report)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(1904, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.סוףמשמרתToolStripMenuItem,
            this.דוחמסכםToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.fileToolStripMenuItem.Text = "קובץ";
            // 
            // סוףמשמרתToolStripMenuItem
            // 
            this.סוףמשמרתToolStripMenuItem.Name = "סוףמשמרתToolStripMenuItem";
            this.סוףמשמרתToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.סוףמשמרתToolStripMenuItem.Text = "סוף משמרת";
            this.סוףמשמרתToolStripMenuItem.Click += new System.EventHandler(this.סוףמשמרתToolStripMenuItem_Click);
            // 
            // דוחמסכםToolStripMenuItem
            // 
            this.דוחמסכםToolStripMenuItem.Name = "דוחמסכםToolStripMenuItem";
            this.דוחמסכםToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.דוחמסכםToolStripMenuItem.Text = "דוח מסכם";
            this.דוחמסכםToolStripMenuItem.Click += new System.EventHandler(this.דוחמסכםToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.exitToolStripMenuItem.Text = "יציאה";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // DGV_Totals
            // 
            this.DGV_Totals.AllowUserToAddRows = false;
            this.DGV_Totals.AllowUserToDeleteRows = false;
            this.DGV_Totals.AllowUserToResizeRows = false;
            this.DGV_Totals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Totals.Location = new System.Drawing.Point(11, 220);
            this.DGV_Totals.Name = "DGV_Totals";
            this.DGV_Totals.Size = new System.Drawing.Size(1851, 712);
            this.DGV_Totals.TabIndex = 1;
            this.DGV_Totals.Visible = false;
            this.DGV_Totals.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGV_Totals_CellBeginEdit);
            this.DGV_Totals.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Totals_CellDoubleClick);
            this.DGV_Totals.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGV_Totals_CellValidating);
            this.DGV_Totals.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.DGV_Totals_RowsAdded);
            // 
            // Lbl_CurrentShift
            // 
            this.Lbl_CurrentShift.AutoSize = true;
            this.Lbl_CurrentShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_CurrentShift.Location = new System.Drawing.Point(864, 70);
            this.Lbl_CurrentShift.Name = "Lbl_CurrentShift";
            this.Lbl_CurrentShift.Size = new System.Drawing.Size(66, 24);
            this.Lbl_CurrentShift.TabIndex = 2;
            this.Lbl_CurrentShift.Text = "label1";
            this.Lbl_CurrentShift.Visible = false;
            // 
            // Btn_Submit
            // 
            this.Btn_Submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Submit.Location = new System.Drawing.Point(868, 961);
            this.Btn_Submit.Name = "Btn_Submit";
            this.Btn_Submit.Size = new System.Drawing.Size(163, 32);
            this.Btn_Submit.TabIndex = 3;
            this.Btn_Submit.Text = "עדכן נתונים";
            this.Btn_Submit.UseVisualStyleBackColor = true;
            this.Btn_Submit.Visible = false;
            this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
            // 
            // DGV_Report
            // 
            this.DGV_Report.AllowUserToAddRows = false;
            this.DGV_Report.AllowUserToDeleteRows = false;
            this.DGV_Report.AllowUserToResizeRows = false;
            this.DGV_Report.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Report.Location = new System.Drawing.Point(12, 220);
            this.DGV_Report.Name = "DGV_Report";
            this.DGV_Report.ReadOnly = true;
            this.DGV_Report.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.DGV_Report.Size = new System.Drawing.Size(1850, 712);
            this.DGV_Report.TabIndex = 4;
            this.DGV_Report.Visible = false;
            this.DGV_Report.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DGV_Report_CellFormatting);
            this.DGV_Report.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.DGV_Report_CellPainting);
            // 
            // Lbl_To
            // 
            this.Lbl_To.AutoSize = true;
            this.Lbl_To.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_To.Location = new System.Drawing.Point(880, 179);
            this.Lbl_To.Name = "Lbl_To";
            this.Lbl_To.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_To.Size = new System.Drawing.Size(91, 19);
            this.Lbl_To.TabIndex = 5;
            this.Lbl_To.Text = "עד תאריך:";
            this.Lbl_To.Visible = false;
            // 
            // Lbl_From
            // 
            this.Lbl_From.AutoSize = true;
            this.Lbl_From.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_From.Location = new System.Drawing.Point(1204, 179);
            this.Lbl_From.Name = "Lbl_From";
            this.Lbl_From.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_From.Size = new System.Drawing.Size(75, 19);
            this.Lbl_From.TabIndex = 6;
            this.Lbl_From.Text = "מתאריך:";
            this.Lbl_From.Visible = false;
            // 
            // DTP_From
            // 
            this.DTP_From.CalendarFont = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DTP_From.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DTP_From.Location = new System.Drawing.Point(998, 174);
            this.DTP_From.Name = "DTP_From";
            this.DTP_From.Size = new System.Drawing.Size(200, 27);
            this.DTP_From.TabIndex = 7;
            this.DTP_From.Visible = false;
            // 
            // DTP_To
            // 
            this.DTP_To.CalendarFont = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DTP_To.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.DTP_To.Location = new System.Drawing.Point(664, 174);
            this.DTP_To.Name = "DTP_To";
            this.DTP_To.Size = new System.Drawing.Size(210, 27);
            this.DTP_To.TabIndex = 8;
            this.DTP_To.Visible = false;
            // 
            // BTN_Send
            // 
            this.BTN_Send.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BTN_Send.Location = new System.Drawing.Point(556, 174);
            this.BTN_Send.Name = "BTN_Send";
            this.BTN_Send.Size = new System.Drawing.Size(91, 29);
            this.BTN_Send.TabIndex = 9;
            this.BTN_Send.Text = "הפק";
            this.BTN_Send.UseVisualStyleBackColor = true;
            this.BTN_Send.Visible = false;
            this.BTN_Send.Click += new System.EventHandler(this.BTN_Send_Click);
            // 
            // Btn_Split
            // 
            this.Btn_Split.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold);
            this.Btn_Split.Location = new System.Drawing.Point(1671, 179);
            this.Btn_Split.Name = "Btn_Split";
            this.Btn_Split.Size = new System.Drawing.Size(191, 32);
            this.Btn_Split.TabIndex = 10;
            this.Btn_Split.Text = "פצל שורה";
            this.Btn_Split.UseVisualStyleBackColor = true;
            this.Btn_Split.Visible = false;
            this.Btn_Split.Click += new System.EventHandler(this.Btn_Split_Click);
            // 
            // Btn_Print
            // 
            this.Btn_Print.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Print.Location = new System.Drawing.Point(12, 175);
            this.Btn_Print.Name = "Btn_Print";
            this.Btn_Print.Size = new System.Drawing.Size(91, 29);
            this.Btn_Print.TabIndex = 11;
            this.Btn_Print.Text = "הדפסה";
            this.Btn_Print.UseVisualStyleBackColor = true;
            this.Btn_Print.Visible = false;
            this.Btn_Print.Click += new System.EventHandler(this.Btn_Print_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Lbl_Shift
            // 
            this.Lbl_Shift.AutoSize = true;
            this.Lbl_Shift.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_Shift.Font = new System.Drawing.Font("Tahoma", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Shift.ForeColor = System.Drawing.Color.Red;
            this.Lbl_Shift.Location = new System.Drawing.Point(683, 99);
            this.Lbl_Shift.Name = "Lbl_Shift";
            this.Lbl_Shift.Size = new System.Drawing.Size(344, 116);
            this.Lbl_Shift.TabIndex = 12;
            this.Lbl_Shift.Text = "label1";
            this.Lbl_Shift.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CuringTotals.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(1904, 1042);
            this.Controls.Add(this.Lbl_Shift);
            this.Controls.Add(this.Btn_Print);
            this.Controls.Add(this.Btn_Split);
            this.Controls.Add(this.BTN_Send);
            this.Controls.Add(this.DTP_To);
            this.Controls.Add(this.DTP_From);
            this.Controls.Add(this.Lbl_From);
            this.Controls.Add(this.Lbl_To);
            this.Controls.Add(this.DGV_Report);
            this.Controls.Add(this.Btn_Submit);
            this.Controls.Add(this.Lbl_CurrentShift);
            this.Controls.Add(this.DGV_Totals);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Curing Totals";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Totals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Report)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.DataGridView DGV_Totals;
        private System.Windows.Forms.Label Lbl_CurrentShift;
        private System.Windows.Forms.Button Btn_Submit;
        private System.Windows.Forms.DataGridView DGV_Report;
        private System.Windows.Forms.ToolStripMenuItem סוףמשמרתToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem דוחמסכםToolStripMenuItem;
        private System.Windows.Forms.Label Lbl_To;
        private System.Windows.Forms.Label Lbl_From;
        private System.Windows.Forms.DateTimePicker DTP_From;
        private System.Windows.Forms.DateTimePicker DTP_To;
        private System.Windows.Forms.Button BTN_Send;
        private System.Windows.Forms.Button Btn_Split;
        private System.Windows.Forms.Button Btn_Print;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label Lbl_Shift;
    }
}

